{
  description = "Website flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11-small";
    systems.url = "github:nix-systems/x86_64-linux";
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.systems.follows = "systems";
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = nixpkgs.legacyPackages.${system};
      in {
        packages = {
          default = self.packages."${system}".vhack.eu;
          vhack.eu = pkgs.stdenv.mkDerivation {
            pname = "vhack.eu";
            version = "1";
            src = ./src;
            nativeBuildInputs = [];
            buildPhase = "";
            installPhase = ''
              install -d $out/
              cp -r . $out/
            '';
          };
        };
        devShells = {
          default = pkgs.mkShell {
            packages = with pkgs; [
              nil
              alejandra
              statix
              shellcheck
              ltex-ls
            ];
          };
        };
      }
    );
}
# vim: ts=2

